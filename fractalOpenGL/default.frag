#version 440 core

#define cx_mul(a, b) vec2(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x)

out vec4 FragColor;
in vec2 gl_FragCoord;

uniform vec2 WindowSize;
uniform dvec2 Offset;
uniform double AspectRatio;
uniform double Zoom;
uniform int IterationCount;

dvec2 uv = (dvec2(((gl_FragCoord - 0.5f) / WindowSize) - 0.5f) * dvec2(AspectRatio, 1.0) / Zoom + Offset);

vec4 getOutsideColor(double asdf, int i) {
	return vec4(vec2(uv), 0.5f, 1.0f);
}


void main()
{
	dvec2 init = uv;
	dvec2 zn = dvec2(0.0f, 0.0f);
	double asdf;

	for(int i = 0; i < IterationCount; i++) {
		zn = cx_mul(zn, zn) + init;
		asdf = zn.x*zn.x + zn.y*zn.y;
		
		if (asdf > 4f) {
			FragColor = getOutsideColor(asdf, 2);
			return;
		}
		
	}
	FragColor = vec4(0f, 0f, 0f, 1.0f);
	
}