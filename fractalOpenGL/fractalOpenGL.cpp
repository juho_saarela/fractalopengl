// fractalOpenGL.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"

#include "glad/glad.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <SFML/OpenGL.hpp>
#include <SFML/Window.hpp>
#include <cstdint>

const std::string loadShaderSourceFile(const char* filename) {
	std::ifstream vertexShaderStream(filename);
	std::stringstream buffer;
	buffer << vertexShaderStream.rdbuf();
	return buffer.str();
}

unsigned int indices[] = {  // note that we start from 0!
	0, 1, 3,   // first triangle
	1, 2, 3    // second triangle
};
float vertices[] = {
	 1.0f,  1.0f, 0.0f,  // top right
	 1.0f, -1.0f, 0.0f,  // bottom right
	-1.0f, -1.0f, 0.0f,  // bottom left
	-1.0f,  1.0f, 0.0f   // top left
};

class Program {
private:
	GLuint EBO;
	GLuint VBO;
	GLuint VAO;

	void printCoords(double x, double y) {
		std::cout << "x: " << x << ", " << "y: " << y << "\n";
		std::cout << "x: ";
		int64_t *b = reinterpret_cast<int64_t*>(&x); // use reinterpret_cast function
		for (int64_t k = 31; k >= 0; k--) // for loop to print out binary pattern
		{
			int64_t bit = ((*b >> k) & 1); // get the copied bit value shift right k times, then and with a 1.
			std::cout << bit; // print the bit.
		}
		std::cout << ", y: ";
		b = reinterpret_cast<int64_t*>(&y); // use reinterpret_cast function
		for (int64_t k = 31; k >= 0; k--) // for loop to print out binary pattern
		{
			int64_t bit = ((*b >> k) & 1); // get the copied bit value shift right k times, then and with a 1.
			std::cout << bit; // print the bit.
		}
		std::cout << "\n";
	}

	void loadShaders(GLint* uniform_WindowSize, GLint* uniform_AspectRatio, GLint* uniform_Offset, GLint* uniform_Zoom, GLint* uniform_IterationCount, GLuint* shaderProgram) {
		const std::string tempstring = loadShaderSourceFile("blank.vert").c_str();
		const char *vertexShaderSource = tempstring.c_str();

		const std::string tempstring2 = loadShaderSourceFile("default.frag").c_str();
		const char *fragmentShaderSource = tempstring2.c_str();

		GLuint vertexShader;
		vertexShader = glCreateShader(GL_VERTEX_SHADER);

		glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
		glCompileShader(vertexShader);

		GLint success;
		char infoLog[512];
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

		if (!success)
		{
			glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		unsigned int fragmentShader;
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
		glCompileShader(fragmentShader);

		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		*shaderProgram = glCreateProgram();

		glAttachShader(*shaderProgram, vertexShader);
		glAttachShader(*shaderProgram, fragmentShader);

		glLinkProgram(*shaderProgram);

		glGetProgramiv(*shaderProgram, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(*shaderProgram, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::PROGRAM::COMPILATION_FAILED\n" << infoLog << std::endl;
		}
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		*uniform_WindowSize = glGetUniformLocation(*shaderProgram, "WindowSize");
		*uniform_AspectRatio = glGetUniformLocation(*shaderProgram, "AspectRatio");
		*uniform_Offset = glGetUniformLocation(*shaderProgram, "Offset");
		*uniform_Zoom = glGetUniformLocation(*shaderProgram, "Zoom");
		*uniform_IterationCount = glGetUniformLocation(*shaderProgram, "IterationCount");
	}

	void createGLShit() {
		glGenBuffers(1, &EBO);

		glGenBuffers(1, &VBO);

		glGenVertexArrays(1, &VAO);

		// ..:: Initialization code :: ..
		// 1. bind Vertex Array Object
		glBindVertexArray(VAO);
		// 2. copy our vertices array in a vertex buffer for OpenGL to use
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		// 3. copy our index array in a element buffer for OpenGL to use
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
		// 4. then set the vertex attributes pointers
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
	}

public:
	int run() {
		int screenWidth = 800;
		int screenHeight = 600;
		double aspectRatio = (double)screenWidth / (double)screenHeight;
		bool fullScreen = false;

		std::cout << "aspectRatio: " << aspectRatio << "\n";
		// create the window

		sf::ContextSettings settings;
		settings.antialiasingLevel = 4;
		sf::VideoMode mode;
		auto style = sf::Style::Fullscreen;

		if (fullScreen) {
			mode = sf::VideoMode::getDesktopMode();
			style = sf::Style::Fullscreen;
		}
		else {
			mode = sf::VideoMode(screenWidth, screenHeight);
			style = sf::Style::Default;
		}

		sf::Window window(mode, "Greased Fractal", style, settings);

		settings = window.getSettings();

		std::cout << "depth bits:" << settings.depthBits << std::endl;
		std::cout << "stencil bits:" << settings.stencilBits << std::endl;
		std::cout << "antialiasing level:" << settings.antialiasingLevel << std::endl;
		std::cout << "version:" << settings.majorVersion << "." << settings.minorVersion << std::endl;
		window.setVerticalSyncEnabled(true);

		// activate the window
		window.setActive(true);

		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(sf::Context::getFunction)))
			//if (!gladLoadGL())
		{
			std::cout << "Failed to initialize GLAD" << std::endl;
			return -1;
		}

		// load resources, initialize the OpenGL states, ...
		GLint uniform_WindowSize;
		GLint uniform_AspectRatio;
		GLint uniform_Offset;
		GLint uniform_Zoom;
		GLint uniform_IterationCount;
		GLuint shaderProgram;
		loadShaders(&uniform_WindowSize, &uniform_AspectRatio, &uniform_Offset, &uniform_Zoom, &uniform_IterationCount, &shaderProgram);
		createGLShit();

		glUseProgram(shaderProgram);

		// run the main loop
		bool running = true;

		double x = -0.0106156, y = 0.638391;
		double zoom = 1.0f;
		int iterationCount = 400;

		glUniform2d(uniform_Offset, x, y);
		glUniform1d(uniform_Zoom, zoom);
		glUniform1i(uniform_IterationCount, iterationCount);

		while (running)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
				x += 0.01f / zoom;
				glUniform2d(uniform_Offset, x, y);
				printCoords(x, y);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
				x -= 0.01f / zoom;
				glUniform2d(uniform_Offset, x, y);
				printCoords(x, y);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
				y += 0.01f / zoom;
				glUniform2d(uniform_Offset, x, y);
				printCoords(x, y);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
				y -= 0.01f / zoom;
				glUniform2d(uniform_Offset, x, y);
				printCoords(x, y);
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add)) {
				zoom *= 1.02f;
				glUniform1d(uniform_Zoom, zoom);
				std::cout << "ZOOM: " << zoom << "\n";
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)) {
				zoom /= 1.02f;
				glUniform1d(uniform_Zoom, zoom);
				std::cout << "ZOOM: " << zoom << "\n";
			}
			// handle events
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed || event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
				{
					// end the program
					running = false;
				}
				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Period) {
					iterationCount *= 2;
					std::cout << "IterationCount: " << iterationCount << "\n";
					glUniform1i(uniform_IterationCount, iterationCount);
				}
				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Comma) {
					iterationCount /= 2;
					if (iterationCount == 0) {
						iterationCount = 1;
					}
					std::cout << "IterationCount: " << iterationCount << "\n";
					glUniform1i(uniform_IterationCount, iterationCount);
				}
				else if (event.type == sf::Event::Resized)
				{
					screenWidth = event.size.width;
					screenHeight = event.size.height;
					// adjust the viewport when the window is resized
					glViewport(0, 0, screenWidth, screenHeight);
					aspectRatio = (double)screenWidth / (double)screenHeight;
					std::cout << "newres: " << screenWidth << ", " << screenHeight << "\n";
					std::cout << "aspectRatio: " << aspectRatio << "\n";
				}
				else if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::F) {
					fullScreen = !fullScreen;
					if (fullScreen) {
						mode = sf::VideoMode::getDesktopMode();
						style = sf::Style::Fullscreen;
					}
					else {
						mode = sf::VideoMode(screenWidth, screenHeight);
						style = sf::Style::Default;
					}
					screenWidth = mode.width;
					screenHeight = mode.height;
					aspectRatio = (double)screenWidth / (double)screenHeight;
					std::cout << "newres: " << screenWidth << ", " << screenHeight << "\n";
					std::cout << "aspectRatio: " << aspectRatio << "\n";
					window.create(mode, "Greased Fractal", style, settings);
					glViewport(0, 0, screenWidth, screenHeight);
					createGLShit();
				}
			}

			// draw...
			// clear the buffers
			glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glUseProgram(shaderProgram);
			glUniform2f(uniform_WindowSize, (GLfloat)screenWidth, (GLfloat)screenHeight);
			glUniform1d(uniform_AspectRatio, aspectRatio);
			glBindVertexArray(VAO);
			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
			glBindVertexArray(0);

			// end the current frame (internally swaps the front and back buffers)
			window.display();
		}
	}
};

int main()
{
	Program prog;
	return prog.run();
}